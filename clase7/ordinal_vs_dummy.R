# carga de datos
library(foreign)
setwd("/home/rstudio/regresion/clase7")
df <- read.dta("crime.dta")

# nivel de pobreza como quartiles (1, 2, 3, 4)
df$poverty_cuartiles <- cut(df$poverty, quantile(df$poverty), c(1, 2, 3, 4), TRUE)

# nivel de pobreza como dummy (todos 0 implica primer cuartil)
df$poverty_c2 <- as.integer(df$poverty_cuartiles == 2)
df$poverty_c3 <- as.integer(df$poverty_cuartiles == 3)
df$poverty_c4 <- as.integer(df$poverty_cuartiles == 4)

# creo un modelo para predecir crimen en funcion de pobreza CUALI ORDINAL
m1 <- lm(crime~poverty_cuartiles, df)
summary(m1) # Adjusted R-squared:  0.1535
extractAIC(m1) # 616.44

# creo un modelo para predecir crimen en funcion de pobreza DUMMIES
m2 <- lm(crime~poverty_c2+poverty_c3+poverty_c4, df)
summary(m2) # Adjusted R-squared 0.1535  
extractAIC(m2) # AIC 616.44


